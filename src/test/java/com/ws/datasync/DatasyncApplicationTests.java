package com.ws.datasync;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DatasyncApplicationTests {
    @Autowired
    @Qualifier("primaryJdbcTemplate")
    private JdbcTemplate primaryTemplate; //目的连接


    @Test
    public void contextLoads() {
        String sql = "INSERT INTO pms_brand VALUES(?,?,?,?,?,?,?)";
        Object[] objects = new Object[]{2L,"华为","https://gulimall-pihao.oss-cn-shenzhen.aliyuncs.com/2021-02-19/3c90a99a-0bcb-40c8-811f-bd16a305f394_63e862164165f483.jpg","皮浩",1,"H",1};
        int update = primaryTemplate.update(sql, objects);
        System.out.println(update);
    }

}
